package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;

import java.util.Arrays;

public class ShowArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final String[] arguments = serviceLocator.getCommandService().getArgs();
        System.out.println(Arrays.toString(arguments));
    }

}
