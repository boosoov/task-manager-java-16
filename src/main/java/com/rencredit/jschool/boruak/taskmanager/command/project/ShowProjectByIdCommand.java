package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

import static com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil.showProject;

public class ShowProjectByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(serviceLocator.getAuthService().getUserId(), id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

}
