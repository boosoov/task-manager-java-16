package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;

import java.util.List;

public class ShowProjectsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show projects list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = serviceLocator.getProjectService().findAll(serviceLocator.getAuthService().getUserId());
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

}
