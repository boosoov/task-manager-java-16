package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.command.user.*;
import com.rencredit.jschool.boruak.taskmanager.command.user.auth.LogOutCommand;
import com.rencredit.jschool.boruak.taskmanager.command.user.auth.LoginCommand;
import com.rencredit.jschool.boruak.taskmanager.command.project.*;
import com.rencredit.jschool.boruak.taskmanager.command.system.*;
import com.rencredit.jschool.boruak.taskmanager.command.task.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    private final ServiceLocator serviceLocator;

    private static final Class[] COMMANDS = new Class[]{
        HelpCommand.class, SystemInfoCommand.class, ShowArgumentsCommand.class,
        ShowCommandsCommand.class, ShowDeveloperInfoCommand.class, ShowVersionCommand.class,
        SystemExitCommand.class, UpdatePasswordCommand.class, ShowProfileCommand.class,
        EditProfileCommand.class, LogOutCommand.class, LoginCommand.class,
        RegistrationCommand.class, UserLockCommand.class, UserUnlockCommand.class,
        UserDeleteCommand.class,

        ClearProjectsCommand.class, CreateProjectCommand.class, RemoveProjectByIdCommand.class,
        RemoveProjectByIndexCommand.class, RemoveProjectByNameCommand.class, ShowProjectByIdCommand.class,
        ShowProjectByIndexCommand.class, ShowProjectByNameCommand.class, ShowProjectsCommand.class,
        UpdateProjectByIdCommand.class, UpdateProjectByIndexCommand.class,

        ClearTasksCommand.class, CreateTaskCommand.class, RemoveTaskByIdCommand.class,
        RemoveTaskByIndexCommand.class, RemoveTaskByNameCommand.class, ShowTaskByIdCommand.class,
        ShowTaskByIndexCommand.class, ShowTaskByNameCommand.class, ShowTasksCommand.class,
        UpdateTaskByIdCommand.class, UpdateTaskByIndexCommand.class,
    };

    public CommandRepository(final ServiceLocator serviceLocator) {
        System.out.println("CommandRepository - ServiceLocator");
        System.out.println(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void registryCommand() {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                registry(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(serviceLocator);
        commands.put(command.name(), command);
    }

    public String[] getCommands() {
        final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().name() != null && !command.getValue().name().isEmpty())
                resultString.append(command.getValue().name());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty())
                resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs() {
        final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().arg() != null && !command.getValue().arg().isEmpty())
                resultString.append(command.getValue().arg());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty())
                resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Map<String, AbstractCommand> getTerminalCommands() {
        return commands;
    }

}
