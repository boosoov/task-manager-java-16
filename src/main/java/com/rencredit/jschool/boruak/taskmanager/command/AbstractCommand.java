package com.rencredit.jschool.boruak.taskmanager.command;

import com.rencredit.jschool.boruak.taskmanager.api.service.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String arg();

    public abstract String name();

    public abstract  String description();

    public abstract void execute();

}
