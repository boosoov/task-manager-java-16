package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class EditProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "edit-profile";
    }

    @Override
    public String description() {
        return "Edit profile.";
    }

    @Override
    public void execute() {
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        final String middleName = TerminalUtil.nextLine();

        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getUserService().editProfileById(userId, email, firstName, lastName, middleName);
        System.out.println(user);
    }

}
