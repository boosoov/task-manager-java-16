package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

import java.util.List;

public interface IUserService {

    User getById(String id);

    User getByLogin(String login);

    List<User> getAll();

    User add(String login, String password);

    User add(String login, String password, String firstName);

    User add(String login, String password, Role role);

    User editProfileById(String id, String firstName);

    User editProfileById(String id, String firstName, String lastName);

    User editProfileById(String id, final String email, String firstName, String lastName, final String middleName);

    User updatePasswordById(String id, String newPassword);

    User removeById(String id);

    User removeByLogin(String login);

    User removeByUser(User user);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
