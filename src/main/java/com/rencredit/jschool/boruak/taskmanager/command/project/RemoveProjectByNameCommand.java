package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class RemoveProjectByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeOneByName(serviceLocator.getAuthService().getUserId(), name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
