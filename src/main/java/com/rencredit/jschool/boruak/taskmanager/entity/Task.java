package com.rencredit.jschool.boruak.taskmanager.entity;

import java.util.UUID;

public class Task extends AbstractEntity {

    private String name;

    private String description;

    private String userId = "";

    public Task() {
    }

    public Task(final String title) {
        this.name = title;
    }

    public Task(final String title, final String description) {
        this.name = title;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
