package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class UserDeleteCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "delete-user";
    }

    @Override
    public String description() {
        return "Delete user";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE USER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
