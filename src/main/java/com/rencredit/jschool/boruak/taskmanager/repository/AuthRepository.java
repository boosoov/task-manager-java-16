package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IAuthRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.User;

public class AuthRepository implements IAuthRepository {

    private String userId;

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void logIn(final User user) {
        userId = user.getId();
    }

    @Override
    public void logOut() {
        userId = null;
    }

}
